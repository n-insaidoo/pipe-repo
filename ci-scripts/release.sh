#!/usr/bin/env bash

set -ex

# import common.sh
source common.sh

IMAGE=$1

##
# Step 1: Generate new version
##
previous_version=$2
# new_version=$(cat version.txt)

# debug "Setup done"
##
# Step 2: Generate CHANGELOG.md
##
# echo "Generating CHANGELOG.md file..."
# semversioner changelog > CHANGELOG.md
# Use new version in the README.md examples
# debug "Replacing old version (${previous_version}) in the README.md with new version (${new_version})"
# echo "Replace version '$previous_version' to '$new_version' in README.md ..."
# sed -i "s/$BITBUCKET_REPO_SLUG:[0-9]*\.[0-9]*\.[0-9]*/$BITBUCKET_REPO_SLUG:$new_version/g" README.md
# # Use new version in the pipe.yml metadata file
# debug "Replacing old version (${previous_version}) in the pipe.yml with new version (${new_version})"
# echo "Replace version '$previous_version' to '$new_version' in pipe.yml ..."
# sed -i "s/$BITBUCKET_REPO_SLUG:[0-9]*\.[0-9]*\.[0-9]*/$BITBUCKET_REPO_SLUG:$new_version/g" pipe.yml

##
# Step 3: Build and push docker image
##

# For these functional tests we don't need versioning

new_version=$2
echo ${DOCKERHUB_PASSWORD} | docker login --username "$DOCKERHUB_USERNAME" --password-stdin

build_and_push_docker_image() {
    version="$1"
    variant="${2:-}"
    echo "Building docker image ${IMAGE}:${version}"
    if [[ -n "$(echo "$version" | grep '-')" ]]; then
        docker build -t ${IMAGE}:${version} -f "Dockerfile.$variant" .
    else
        docker build -t ${IMAGE}:${version} .
    fi
    docker push ${IMAGE}:${version}
}

version_commit() {
    version="$1"
    # Use new version in the pipe.yml metadata file
    debug "Replacing old version in the pipe.yml with new version (${version})"
    echo "Replace version to '$version' in pipe.yml..."
    sed -i "s/image:.*/image: nanainsaidoo95\/pipe-repo:${version}/g" pipe.yml
    debug "Committing updated files for version ${version} in branch ${BITBUCKET_BRANCH}"
    echo "Committing updated files to the repository..."
    git add .
    git commit -m "Update files for new version '${version}' [skip ci]"
    git push origin ${BITBUCKET_BRANCH}
}

version_tag() {
    ##
    # Step 5: Tag the repository
    ##
    version="$1"
    debug "Tagging..."
    echo "Tagging for release ${version}"
    git tag -a -f -m "Tagging for release ${version}" "${version}"
    git push origin ${version}
}


##
# Step 4: Commit back to the repository, covering variants first and then the main version
##
VARIANTS="dotnet"
for variant in $VARIANTS; do
    build_and_push_docker_image "$new_version-$variant" "$variant"
    version_commit "$new_version-$variant"
    version_tag "$new_version-$variant"
done

build_and_push_docker_image "$new_version"
version_commit "$new_version"
version_tag "$new_version"

