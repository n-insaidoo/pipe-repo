#!/usr/bin/env bats


setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="nanainsaidoo95/pipe-repo"}

  echo "Building image..."
  run docker build -t ${DOCKER_IMAGE} .
}

teardown() {
    echo "Teardown happens after each test."
}

@test "Dummy test" {
    run docker run \
        -e NAME="baz" \
        -e SURNAME="taz" \
        -e AGE=34 \
        ${DOCKER_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}


