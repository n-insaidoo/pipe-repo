FROM meterian/cli-canary:latest-java

COPY pipe/*.sh /root
RUN chmod a+x /root/*.sh && echo "java" > variant.txt

ENTRYPOINT ["/root/pipe.sh"]
