#!/bin/sh

set -e
set -x

BINSTASH="$BITBUCKET_PIPE_STORAGE_DIR/binstash"

# TODO figure what could possibly go wrong if an image variant which is not supposed to prep is invoked
# Could have all the bin prep blocks into separate functions in separate file that we source at the beginning of the script
# Could move all the common dirs copies like /etc /usr/bin in a upper block where they get created once and are available for a given variant
if [[ "$PREP" == "true" ]]; then
    mkdir -p $BINSTASH

    current_variant="$(cat /root/variant.txt)" 
    if [[ "$current_variant" == "dotnet" ]]; then
        # ALWAYS USE FULL PATHS HERE
        mkdir -p "$BINSTASH/usr/lib/"
        cp -rP /usr/lib/dotnet "$BINSTASH/usr/lib/dotnet"
        cp -P /usr/lib/libintl.so.8 "$BINSTASH/usr/lib/libintl.so.8"
        cp  /usr/lib/libintl.so.8.1.7 "$BINSTASH/usr/lib/libintl.so.8.1.7"

        mkdir -p "$BINSTASH/usr/bin/"
        cp -P /usr/bin/dotnet "$BINSTASH/usr/bin/dotnet"

        mkdir -p "$BINSTASH/etc/apk/"
        mkdir -p "$BINSTASH/lib/apk/db/"
        cp /etc/apk/world "$BINSTASH/etc/apk/world"
        cp /lib/apk/db/installed "$BINSTASH/lib/apk/db/installed" 
        cp /lib/apk/db/scripts.tar "$BINSTASH/lib/apk/db/scripts.tar" 

        echo "Prep for $current_variant successfully completed!"
    fi
else
    # swift binary "install" if the BINSTASH exists; in this case we know that some prep was done before
    if [[ -d "$BINSTASH" ]]; then
        # exporting some envs that some of the tools use, TODO could organise it bette whereas only specific languages that've been prepped have their env exported
        export DOTNET_SYSTEM_GLOBALIZATION_INVARIANT="1"

        for folder in $(ls $BINSTASH); do
            # this copy only copies what's missing at the destination leaving everything else intact
            cp -rP "$BINSTASH/$folder" "/"
        done
    fi

    echo
    echo "Printing PATH"
    echo "$PATH"
    echo

    echo "Checking contents of /usr/bin"
    ls -l /usr/bin

    echo Checking all installed tools
    set +e
    which java 2>&1 >> /dev/null
    if [[ $? -eq 0 ]]; then 
        java --version
        mvn --version
        gradle --version
    fi
    which dotnet 2>&1 >> /dev/null
    if [[ $? -eq 0 ]]; then
        dotnet --version
    else
        echo Trying to make dotnet work regardless
        if [[ -d /usr/lib/dotnet ]]; then
            echo "Dotnet binaries are present on the system"
            echo "Proceeding to create symlink to use dotnet"
            ln -s /usr/lib/dotnet/dotnet /usr/bin/dotnet
            dotnet --version
        else
            echo "Dotnet binaries were not found on the system"
        fi
    fi
    # which pipenv
    # which gem
    # which composer
fi