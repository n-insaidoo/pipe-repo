# Bitbucket Pipelines Pipe: My Demo Pipe

This is pipe is simply a demo to familiarize with Bitbucket pipelines.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: n-insaidoo/pipe-repo
    variables:
      NAME: '<string>'
      SURNAME: '<string>'
      AGE: '<string>'
```

## Variables

| Variable | Usage |
|----------|-------|
| NAME | Your first name |
| SURNAME | Your last name |
| AGE | Your age |

## Details

This is pipe is simply a demo to familiarize with Bitbucket pipelines.

## Prerequisites

N/A

## Examples

**General snippet**

```yaml
script:
  - pipe: n-insaidoo/pipe-repo
    variables:
      NAME: '<string>'
      SURNAME: '<string>'
      AGE: '<string>'
```
***Snippet for debug**

```yaml
script:
  - pipe: n-insaidoo/pipe-repo
    variables:
      NAME: '<string>'
      SURNAME: '<string>'
      AGE: '<string>'
      DEBUG: true
```

## Support

Contact Nana at nana.insaidoo@meterian.com for support